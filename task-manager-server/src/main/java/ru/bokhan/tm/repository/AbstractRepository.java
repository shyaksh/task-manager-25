package ru.bokhan.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bokhan.tm.api.IRepository;
import ru.bokhan.tm.dto.AbstractEntityDTO;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractRepository<E extends AbstractEntityDTO> implements IRepository<E> {

    @NotNull
    protected final List<E> entities = new ArrayList<>();

    @Override
    public void add(@NotNull final E e) {
        entities.add(e);
    }

    @Override
    public void addAll(@NotNull final List<E> list) {
        entities.addAll(list);
    }

    @Nullable
    @Override
    public E remove(@NotNull E e) {
        @Nullable E found = findById(e.getId());
        if (found == null) return null;
        entities.remove(found);
        return e;
    }

    @Override
    public void load(@NotNull final List<E> list) {
        clear();
        addAll(list);
    }

    @Override
    public void clear() {
        entities.clear();
    }

    @NotNull
    @Override
    public List<E> findAll() {
        return entities;
    }

    @Override
    public E findById(@NotNull String id) {
        for (@Nullable final E entity : entities) {
            if (entity == null) continue;
            if (id.equals(entity.getId()))
                return entity;
        }
        return null;
    }

}
