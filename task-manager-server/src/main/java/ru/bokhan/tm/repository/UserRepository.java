package ru.bokhan.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bokhan.tm.api.repository.IUserRepository;
import ru.bokhan.tm.dto.UserDTO;

public final class UserRepository extends AbstractRepository<UserDTO> implements IUserRepository {

    @Nullable
    @Override
    public UserDTO findByLogin(@NotNull final String login) {
        for (@Nullable final UserDTO user : entities) {
            if (user == null) continue;
            if (login.equals(user.getLogin())) return user;
        }
        return null;
    }

    @Nullable
    @Override
    public UserDTO removeById(@NotNull final String id) {
        @Nullable final UserDTO user = findById(id);
        if (user == null) return null;
        return remove(user);
    }

    @Nullable
    @Override
    public UserDTO removeByLogin(@NotNull final String login) {
        @Nullable final UserDTO user = findByLogin(login);
        if (user == null) return null;
        return remove(user);
    }

}