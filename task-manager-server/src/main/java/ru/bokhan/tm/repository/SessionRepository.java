package ru.bokhan.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bokhan.tm.api.repository.ISessionRepository;
import ru.bokhan.tm.dto.SessionDTO;

import java.util.ArrayList;
import java.util.List;

public final class SessionRepository extends AbstractRepository<SessionDTO> implements ISessionRepository {

    @NotNull
    @Override
    public List<SessionDTO> findByUserId(@NotNull final String userId) {
        @NotNull final List<SessionDTO> sessions = findAll();
        @NotNull final List<SessionDTO> result = new ArrayList<>();
        for (@Nullable final SessionDTO session : sessions) {
            if (session == null) continue;
            if (userId.equals(session.getUserId())) result.add((session));
        }
        return result;
    }

    @Override
    public void removeByUserId(@NotNull final String userId) {
        final List<SessionDTO> sessions = findByUserId(userId);
        for (final SessionDTO session : sessions) remove(session);
    }

}
