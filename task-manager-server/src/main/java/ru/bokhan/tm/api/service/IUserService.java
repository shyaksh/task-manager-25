package ru.bokhan.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bokhan.tm.api.IService;
import ru.bokhan.tm.dto.UserDTO;
import ru.bokhan.tm.enumerated.Role;

public interface IUserService extends IService<UserDTO> {

    @Nullable
    UserDTO create(@Nullable String login, @Nullable String password);

    @NotNull
    UserDTO create(@Nullable String login, @Nullable String password, @Nullable String email);

    @NotNull
    UserDTO create(@Nullable String login, @Nullable String password, @Nullable Role role);

    @Nullable
    UserDTO findById(@Nullable String id);

    @Nullable
    UserDTO findByLogin(@Nullable String login);

    @Nullable
    UserDTO removeById(@Nullable String id);

    @Nullable
    UserDTO removeByLogin(@Nullable String login);

    @NotNull
    UserDTO updateById(
            @Nullable String id, String login,
            @Nullable String firstName, @Nullable String lastName, @Nullable String middleName,
            @Nullable String email
    );

    @NotNull
    UserDTO updatePasswordById(@Nullable String id, @Nullable String password);

    @Nullable
    UserDTO lockUserByLogin(@Nullable String login);

    @Nullable
    UserDTO unlockUserByLogin(@Nullable String login);

}
