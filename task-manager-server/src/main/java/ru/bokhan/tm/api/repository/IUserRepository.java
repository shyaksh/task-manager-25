package ru.bokhan.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bokhan.tm.api.IRepository;
import ru.bokhan.tm.dto.UserDTO;

public interface IUserRepository extends IRepository<UserDTO> {

    @Nullable
    UserDTO findByLogin(@NotNull String login);

    @Nullable
    UserDTO removeById(@NotNull String id);

    @Nullable
    UserDTO removeByLogin(@NotNull String login);

}
