package ru.bokhan.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bokhan.tm.dto.AbstractEntityDTO;

import java.util.List;

public interface IService<E extends AbstractEntityDTO> {

    @NotNull
    List<E> findAll();

    void clear();

    void load(@Nullable List<E> list);

    @Nullable
    E remove(@Nullable E e);

}
