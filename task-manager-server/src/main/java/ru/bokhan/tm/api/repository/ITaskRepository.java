package ru.bokhan.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bokhan.tm.api.IRepository;
import ru.bokhan.tm.dto.TaskDTO;

import java.util.List;

public interface ITaskRepository extends IRepository<TaskDTO> {

    void add(@NotNull String userId, @NotNull TaskDTO task);

    void remove(@NotNull String userId, @NotNull TaskDTO task);

    void clear(@NotNull String userId);

    @NotNull
    List<TaskDTO> findAll(@NotNull String userId);

    @Nullable
    TaskDTO findById(@NotNull String userId, @NotNull String id);

    @Nullable
    TaskDTO findByIndex(@NotNull String userId, @NotNull Integer index);

    @Nullable
    TaskDTO findByName(@NotNull String userId, @NotNull String name);

    @Nullable
    TaskDTO removeById(@NotNull String userId, @NotNull String id);

    @Nullable
    TaskDTO removeByIndex(@NotNull String userId, @NotNull Integer index);

    @Nullable
    TaskDTO removeByName(@NotNull String userId, @NotNull String name);

}