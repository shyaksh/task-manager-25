package ru.bokhan.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.bokhan.tm.api.service.IPropertyService;

public class PropertyServiceTest {

    @NotNull
    final IPropertyService service = new PropertyService();

    @Before
    public void setUp() {
        service.init();
    }


    @Test
    public void init() {
        getServerHost();
    }

    @Test
    public void getServerHost() {
        Assert.assertNotNull(service.getServerHost());
    }

    @Test
    public void getServerPort() {
        Assert.assertNotNull(service.getServerPort());
    }

    @Test
    public void getSessionSalt() {
        Assert.assertNotNull(service.getSessionSalt());
    }

    @Test
    public void getSessionCycle() {
        Assert.assertNotNull(service.getSessionSalt());
    }

}