package ru.bokhan.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.junit.rules.ExpectedException;
import ru.bokhan.tm.api.repository.IUserRepository;
import ru.bokhan.tm.api.service.IUserService;
import ru.bokhan.tm.dto.UserDTO;
import ru.bokhan.tm.exception.empty.EmptyIdException;
import ru.bokhan.tm.exception.empty.EmptyLoginException;
import ru.bokhan.tm.exception.incorrect.IncorrectDataFileException;
import ru.bokhan.tm.marker.UnitCategory;
import ru.bokhan.tm.repository.UserRepository;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.fail;
import static ru.bokhan.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public class UserServiceTest {

    @NotNull
    private final IUserRepository repository = new UserRepository();

    @NotNull
    private final IUserService service = new UserService(repository);

    @Rule
    @NotNull
    public final ExpectedException thrown = ExpectedException.none();


    @Before
    public void setUp() {
        repository.addAll(USER_LIST);
    }

    @After
    public void tearDown() {
        repository.clear();
    }

    @Test
    public void findAll() {
        Assert.assertEquals(USER_LIST, service.findAll());
    }

    @Test
    public void clear() {
        service.clear();
        Assert.assertTrue(repository.findAll().isEmpty());
    }

    @Test
    public void load() {
        service.load(USER_LIST);
        Assert.assertEquals(USER_LIST, repository.findAll());
        thrown.expect(IncorrectDataFileException.class);
        service.load(null);
    }

    @Test
    public void remove() {
        @NotNull final List<UserDTO> list = new ArrayList<>(repository.findAll());
        @Nullable final UserDTO removed = service.remove(USER1);
        Assert.assertEquals(USER1, removed);
        list.remove(USER1);
        Assert.assertEquals(list, repository.findAll());
        Assert.assertNull(service.remove(null));
        Assert.assertNull(service.remove(USER1));
    }

    @Test
    public void findById() {
        Assert.assertEquals(
                service.findById(USER1.getId()),
                USER1
        );
        thrown.expect(EmptyIdException.class);
        service.findById(null);
    }

    @Test
    public void findByLogin() {
        Assert.assertEquals(
                service.findByLogin(USER1.getLogin()),
                USER1
        );
        thrown.expect(EmptyLoginException.class);
        service.findByLogin(null);
    }

    @Test
    public void removeById() {
        Assert.assertTrue(service.findAll().contains(USER1));
        service.removeById(USER1.getId());
        Assert.assertFalse(service.findAll().contains(USER1));
    }

    @Test
    public void create() {
        repository.clear();
        service.create(USER1.getLogin(), "password"
        );
        Assert.assertNotNull(repository.findByLogin(USER1.getLogin()));
    }

    @Test
    public void createWithEmail() {
        repository.clear();
        service.create(USER1.getLogin(), "password", USER1.getEmail()
        );
        @Nullable final UserDTO created = repository.findByLogin(USER1.getLogin());
        if (created == null) fail();
        Assert.assertEquals(USER1.getEmail(), created.getEmail());
        Assert.assertEquals(USER1.getLogin(), created.getLogin());
    }

    @Test
    public void createWithRole() {
        repository.clear();
        service.create(USER1.getLogin(), "password", USER1.getRole()
        );
        @Nullable final UserDTO created = repository.findByLogin(USER1.getLogin());
        if (created == null) fail();
        Assert.assertEquals(USER1.getLogin(), created.getLogin());
        Assert.assertEquals(USER1.getRole(), created.getRole());
    }

    @Test
    public void updateById() {
        repository.clear();
        @NotNull final UserDTO actual = new UserDTO();
        actual.setId(ADMIN1.getId());
        repository.add(actual);
        service.updateById(
                actual.getId(),
                ADMIN1.getLogin(),
                ADMIN1.getFirstName(),
                ADMIN1.getLastName(),
                ADMIN1.getMiddleName(),
                ADMIN1.getEmail()
        );
        assertThat(service.findById(actual.getId())).isEqualToIgnoringGivenFields(
                ADMIN1, "passwordHash", "role"
        );
    }

    @Test
    public void updatePasswordById() {
        repository.clear();
        @NotNull final UserDTO user1 = new UserDTO();
        user1.setId(USER1.getId());
        repository.add(user1);
        @NotNull final UserDTO user2 = new UserDTO();
        user2.setId(USER2.getId());
        repository.add(user2);
        service.updatePasswordById(USER1.getId(), "password");
        service.updatePasswordById(USER2.getId(), "password");
        Assert.assertEquals(user1.getPasswordHash(), user2.getPasswordHash());
    }

    @Test
    public void lockUserByLogin() {
        repository.clear();
        @NotNull final UserDTO user1 = new UserDTO();
        user1.setLogin(USER1.getLogin());
        user1.setLocked(false);
        repository.add(user1);
        service.lockUserByLogin(USER1.getLogin());
        Assert.assertTrue(user1.getLocked());
    }

    @Test
    public void unlockUserByLogin() {
        repository.clear();
        @NotNull final UserDTO user1 = new UserDTO();
        user1.setLogin(USER1.getLogin());
        user1.setLocked(true);
        repository.add(user1);
        service.unlockUserByLogin(USER1.getLogin());
        Assert.assertFalse(user1.getLocked());
    }

    @Test
    public void removeByLogin() {
        Assert.assertTrue(repository.findAll().contains(USER1));
        service.removeByLogin(USER1.getLogin());
        Assert.assertFalse(repository.findAll().contains(USER1));
    }

}