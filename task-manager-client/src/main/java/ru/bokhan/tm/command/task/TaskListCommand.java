package ru.bokhan.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bokhan.tm.command.AbstractCommand;
import ru.bokhan.tm.endpoint.SessionDTO;
import ru.bokhan.tm.endpoint.TaskDTO;
import ru.bokhan.tm.exception.security.AccessDeniedException;

import java.util.List;

public final class TaskListCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "task-list";
    }

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Show task list";
    }

    @Override
    public void execute() {
        @Nullable final SessionDTO session = endpointLocator.getCurrentSession();
        if (session == null) throw new AccessDeniedException();
        System.out.println("[LIST TASKS]");
        @NotNull final List<TaskDTO> tasks = endpointLocator.getTaskEndpoint().findTaskAll(session);
        for (@Nullable final TaskDTO task : tasks) System.out.println(task);
        System.out.println("[OK]");
    }

}
