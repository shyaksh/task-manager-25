package ru.bokhan.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bokhan.tm.command.AbstractCommand;
import ru.bokhan.tm.endpoint.ProjectDTO;
import ru.bokhan.tm.endpoint.SessionDTO;
import ru.bokhan.tm.exception.security.AccessDeniedException;

import java.util.List;

public final class ProjectListCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "project-list";
    }

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Show project list";
    }

    @Override
    public void execute() {
        @Nullable final SessionDTO session = endpointLocator.getCurrentSession();
        if (session == null) throw new AccessDeniedException();
        System.out.println("[LIST PROJECTS]");
        @NotNull final List<ProjectDTO> projects = endpointLocator.getProjectEndpoint().findProjectAll(session);
        for (@Nullable final ProjectDTO project : projects) System.out.println(project);
        System.out.println("[OK]");
    }

}
