package ru.bokhan.tm;

import org.jetbrains.annotations.NotNull;
import ru.bokhan.tm.bootstrap.Bootstrap;

public final class Client {

    public static void main(String[] args) {
        @NotNull final Bootstrap bootstrap = new Bootstrap();
        bootstrap.run(args);
    }

}
